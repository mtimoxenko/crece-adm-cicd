#!/bin/bash

# Function to fetch and select a Docker image tag from Docker Hub
choose_tag_from_dockerhub() {
    local repository=$1
    echo "Fetching tags for $repository..." >&2
    
    # Fetch tags from Docker Hub
    local tags=$(curl -s "https://hub.docker.com/v2/repositories/${repository}/tags/?page_size=100" | jq -r '.results|.[]|.name')
    
    # Check if we got any tags
    if [ -z "$tags" ]; then
        echo "No tags found for repository $repository. Please check the repository name and try again." >&2
        exit 1
    fi

    echo "Available tags for $repository:" >&2
    local i=1
    local options=()
    for tag in $tags; do
        echo "$i) $tag" >&2
        options+=("$tag")
        let "i+=1"
    done

    # Ask the user to choose a tag with a 5-second timeout
    echo "Enter the number of the tag you want to use (automatically selects the first option after 5 seconds):" >&2
    read -t 5 tag_choice || echo "Timeout reached, automatically selecting the first option." >&2
    
    # If no input is provided (or timeout occurs), default to the first option
    if [ -z "$tag_choice" ]; then
        selected_tag=${options[0]}
    else
        selected_tag=${options[$tag_choice-1]}
    fi

    if [ -z "$selected_tag" ]; then
        echo "Invalid selection. Exiting." >&2
        exit 1
    fi

    # Only the selected tag is echoed to stdout, everything else is directed to stderr
    echo "$selected_tag"
}

run_application() {
    echo "Select a tag for the first frontend image:"
    FRONTEND1_TAG=$(choose_tag_from_dockerhub "fr3m3n/crece-adm-frontend")
    FRONTEND1_IMAGE="fr3m3n/crece-adm-frontend:$FRONTEND1_TAG"

    echo "Select a tag for the second frontend image:"
    FRONTEND2_TAG=$(choose_tag_from_dockerhub "fr3m3n/crece-adm-frontend2")
    FRONTEND2_IMAGE="fr3m3n/crece-adm-frontend2:$FRONTEND2_TAG"

    echo "Select a tag for the backend image:"
    BACKEND_TAG=$(choose_tag_from_dockerhub "fr3m3n/crece-adm-backend")
    BACKEND_IMAGE="fr3m3n/crece-adm-backend:$BACKEND_TAG"

    # Use the captured tags to generate the docker-compose.yml
    cat <<EOF > docker-compose.yml
version: '3.8'
services:
  frontend1:
    container_name: crece-adm-frontend1
    image: $FRONTEND1_IMAGE
    ports:
      - "5000:80"
    depends_on:
      - backend
    networks:
      - crece_adm_network

  frontend2:
    container_name: crece-adm-frontend2
    image: $FRONTEND2_IMAGE
    ports:
      - "5001:80"
    depends_on:
      - backend
    networks:
      - crece_adm_network

  backend:
    container_name: crece-adm-backend
    image: $BACKEND_IMAGE
    ports:
      - "8080:8080"
    networks:
      - crece_adm_network

networks:
  crece_adm_network:
    driver: bridge

EOF

    echo "docker-compose.yml has been generated with the selected images."

    # Run Docker Compose up
    docker-compose up -d
}

# Function to stop the application
stop_application() {
    docker-compose down
}

# Main logic based on the command-line argument
case "$1" in
    up)
        run_application
        ;;
    down)
        stop_application
        ;;
    *)
        echo "Usage: $0 {up|down}"
        exit 1
        ;;
esac
