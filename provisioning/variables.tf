# ==========================================
# Scope: Variables definition
# Author: Maximo Timochenko
# Date: 16.04.24
# Version: 1.0
# ==========================================

variable "db_username" {
  description = "Database Username"
  type        = string
}

variable "db_password" {
  description = "Database Password"
  type        = string
}
