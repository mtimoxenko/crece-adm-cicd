# ==========================================
# Scope: Amazon RDS Instance
# Author: Maximo Timochenko
# Date: 16.04.24
# Version: 1.0
# ==========================================

resource "aws_db_instance" "crece_adm_db" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t3.micro"
  identifier             = "crece-adm-db"
  username               = var.db_username
  password               = var.db_password
  parameter_group_name   = "default.mysql8.0"
  db_subnet_group_name   = aws_db_subnet_group.crece_adm_db_subnet_group.name
  vpc_security_group_ids = [aws_security_group.rds_sg.id]

  tags = {
    Name = "CreceADMDB"
  }
}

resource "aws_db_subnet_group" "crece_adm_db_subnet_group" {
  name = "crece-adm-db-subnet-group"
  subnet_ids = [
    aws_subnet.crece_adm_private_subnet.id,
    aws_subnet.crece_adm_private_subnet_az2.id # Including the new subnet
  ]

  tags = {
    Name = "crece_adm_db_subnet_group"
  }
}


output "db_endpoint" {
  value = aws_db_instance.crece_adm_db.endpoint
}
