# ==========================================
# Scope: EC2 instance
# Author: Maximo Timochenko
# Date: 16.04.24
# Version: 1.0
# ==========================================

resource "aws_instance" "web" {
  ami                    = "ami-0fc5d935ebf8bc3bc" # Ubuntu Server 22.04 LTS
  instance_type          = "t2.micro"
  key_name               = "crece-adm-ec2"
  subnet_id              = aws_subnet.crece_adm_public_subnet.id
  vpc_security_group_ids = [aws_security_group.ec2_sg.id]

  tags = {
    Name = "crece-adm-app"
  }
}

output "ec2_public_ip" {
  description = "Public IP of the EC2 instance"
  value       = aws_instance.web.public_ip
}